import React, { useCallback, useState } from "react";
import {
	View,
	Text,
	StyleSheet,
	TextInput,
	ScrollView,
	Button,
} from "react-native";
import { useDispatch } from "react-redux";
import * as placesActions from "../store/places-action";
import ImgPicker from "../components/ImagePicker";
import LocationPicker from "../components/LocationPicker";

const NewPlaceScreen = (props) => {
	const [titleValue, setTitleValue] = useState("");
	const [selectedImage, setSelectedImage] = useState();
	const [selectedLocation, setSelectedLocation] = useState();
	const dispatch = useDispatch();
	const titleChangeHandler = (text) => {
		//you could add validation
		setTitleValue(text);
	};

	const savePlaceHandler = () => {
		dispatch(placesActions.addPlace(titleValue, selectedImage,selectedLocation));
		props.navigation.goBack();
	};

	const locationPickedHandler = useCallback((location) => {
		// console.log(location, "/////////////////////////////////////");
		setSelectedLocation(location);
	});

	const imageTakenHandler = (imageTaken) => {
		setSelectedImage(imageTaken);
	};
	return (
		<ScrollView>
			<View style={styles.form}>
				<Text style={styles.label}>Title</Text>
				<TextInput
					style={styles.textInput}
					onChangeText={titleChangeHandler}
					value={titleValue}
				/>
				<ImgPicker onImageTaken={imageTakenHandler} />
				<LocationPicker
					navigation={props.navigation}
					onPickedLocation={locationPickedHandler}
				/>
				<Button
					title="Save"
					onPress={savePlaceHandler}
					color={"black"}
				/>
			</View>
		</ScrollView>
	);
};

NewPlaceScreen.navigationOptions = (navData) => {
	return {
		headerTitle: "Add Place",
	};
};

const styles = StyleSheet.create({
	form: {
		margin: 30,
	},
	label: {
		fontSize: 18,
		marginBottom: 15,
	},
	textInput: {
		borderBottomColor: "#ccc",
		borderBottomWidth: 2,
		marginBottom: 15,
		padding: 4,
	},
});

export default NewPlaceScreen;
