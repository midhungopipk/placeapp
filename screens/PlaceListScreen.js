import React, { useEffect } from "react";
import { View, Text, StyleSheet, Platform, FlatList } from "react-native";
import { HeaderButtons, Item } from "react-navigation-header-buttons";
import { useDispatch, useSelector } from "react-redux";
import HeaderButton from "../components/HeaderButton";
import PlaceItem from "../components/PlaceItem";
import * as placesActions from "../store/places-action";

const PlaceListScreen = (props) => {
	const places = useSelector((state) => state.places.places);
	const dispatch = useDispatch();

	useEffect(() => {
		const willFocusSub = props.navigation.addListener(
			"willFocus",
			async () => {
				dispatch(placesActions.loadPlaces());
			}
		);
		return () => {
			willFocusSub.remove();
		};
	}, [dispatch]);
	return (
		<FlatList
			data={places}
			keyExtractor={(item) => item.id}
			renderItem={(itemData) => (
				<PlaceItem
					onSelect={() => {
						props.navigation.navigate("PlaceDetail", {
							placeTitle: itemData.item.title,
							placeId: itemData.item.id,
						});
					}}
					image={itemData.item.imageUri}
					title={itemData.item.title}
					address={itemData.item.address}
				/>
			)}
		/>
	);
};

PlaceListScreen.navigationOptions = (navData) => {
	return {
		headerTitle: "All Places",
		headerRight: () => {
			return (
				<HeaderButtons HeaderButtonComponent={HeaderButton}>
					<Item
						title="Add Places"
						iconName={
							Platform.OS === "android" ? "md-add" : "ios-add"
						}
						onPress={() => {
							navData.navigation.navigate("NewPlace");
						}}
					/>
				</HeaderButtons>
			);
		},
	};
};

const styles = StyleSheet.create({
	screen: {
		flex: 1,
		alignItems: "center",
		justifyContent: "center",
	},
});

export default PlaceListScreen;
