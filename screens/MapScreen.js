import React, { useCallback, useEffect, useState } from "react";
import { View, Text, StyleSheet } from "react-native";
import MapView, { Marker } from "react-native-maps";
import HeaderButton from "../components/HeaderButton";
import { HeaderButtons, Item } from "react-navigation-header-buttons";

const MapScreen = (props) => {
	
	const initialLocation = props.navigation.getParam("initialLocation");
	const readonly = props.navigation.getParam("readonly");

	const [selectedLocation, setSelectedLocation] = useState(initialLocation);
	const mapRegion = {
		latitude: initialLocation ? initialLocation.lat : 37.78,
		longitude: initialLocation ? initialLocation.lng : -122.43,
		latitudeDelta: 0.0922,
		longitudeDelta: 0.0421,
	};
	const selectLocationHandler = (event) => {
		// console.log(event, "...............................");
		if (readonly) {
			return;
		}
		setSelectedLocation({
			lat: event.nativeEvent.coordinate.latitude,
			lng: event.nativeEvent.coordinate.longitude,
		});
	};
	let markerCoordinates;
	if (selectedLocation) {
		markerCoordinates = {
			latitude: selectedLocation.lat,
			longitude: selectedLocation.lng,
		};
	}
	const savePickedLocationHandler = useCallback(() => {
		if (!selectedLocation) {
			return;
		}
		props.navigation.navigate("NewPlace", {
			pickedLocation: selectedLocation,
		});
	}, [selectedLocation]);
	useEffect(() => {
		props.navigation.setParams({ saveLocation: savePickedLocationHandler });
	}, [savePickedLocationHandler]);
	return (
		<MapView
			style={styles.map}
			region={mapRegion}
			onPress={selectLocationHandler}
		>
			{markerCoordinates && (
				<Marker
					title="Picked Location"
					coordinate={markerCoordinates}
				></Marker>
			)}
		</MapView>
	);
};

MapScreen.navigationOptions = (navData) => {
	const saveFn = navData.navigation.getParam("saveLocation");
	const readonly = navData.navigation.getParam("readonly");
	if (readonly) {
		return {};
	}
	return {
		headerRight: () => {
			return (
				<HeaderButtons HeaderButtonComponent={HeaderButton}>
					<Item
						title="Save"
						iconName={
							Platform.OS === "android" ? "md-save" : "ios-save"
						}
						onPress={saveFn}
					/>
				</HeaderButtons>
			);
		},
	};
};
const styles = StyleSheet.create({
	map: {
		flex: 1,
	},
});

export default MapScreen;
