import { createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";

import PlaceListScreen from "../screens/PlaceListScreen";
import PlaceDetailScreen from "../screens/PlaceDatailScreen";
import NewPlaceScreen from "../screens/NewPlaceScreen";
import MapScreen from "../screens/MapScreen";
import { Platform } from "react-native";

const PlaceNavigator = createStackNavigator(
	{
		Places: PlaceListScreen,
		PlaceDetail: PlaceDetailScreen,
		NewPlace: NewPlaceScreen,
		Map: MapScreen,
	},
	{
		defaultNavigationOptions: {
			headerStyle: {
				backgroundColor: Platform.OS === "android" ? "darkgreen" : "",
			},
			headerTintColor: Platform.OS === "android" ? "white" : "darkgreen",
		},
	}
);

export default createAppContainer(PlaceNavigator);
