import * as FileSystem from "expo-file-system";
import { fetchPlaces, insertPlace } from "../helpers/db";
import ENV from "../env";

export const ADD_PLACE = "ADD_PLACE";
export const SET_PLACES = "SET_PLACES";

export const addPlace = (title, image, location) => {
	return async (dispatch) => {
		//for enabling this google api key you need a project with billing account (credit card) . below api is not working .//below fetch is for address formating
		const response = await fetch(
			`https://maps.googleapis.com/maps/api/geocode/json?latlng=${location.lat},${location.lng}&amp;key=${ENV.googleApiKey}`
		);
		if (!response.ok) {
			throw new Error("Something went wrong!");
		}
		const resData = await response.json();
		// console.log(resData,'resDattttttaaaaaaaaaaaaaaaaaaaaaa');

		// const address = resData.result[0].formatted_address;  <<<<<--------//use this address to get formated readable address....................

		const fileName = image.split("/").pop();
		const newPath = FileSystem.documentDirectory + fileName;
		try {
			await FileSystem.moveAsync({ from: image, to: newPath });
			const dbResult = await insertPlace(
				title,
				newPath,
				"Dummy address", //use the above mentioned(commented) address here to get readable address format
				location.lat,
				location.lng
			);
			// console.log(dbResult, "//////////////////");
			dispatch({
				type: ADD_PLACE,
				placeData: {
					id: dbResult.insertId,
					title: title,
					imageUri: newPath,
					address: dbResult.address,
					coords: {
						lat: location.lat,
						lng: location.lng,
					},
				},
			});
			// console.log(dbResult, 'kkkkkkkkkkkkkkkkkkkkkkkkkkkkk')
		} catch (err) {
			console.log(err);
			throw err;
		}
	};
};

export const loadPlaces = () => {
	return async (dispatch) => {
		try {
			const dbResult = await fetchPlaces();
			// console.log(dbResult.rows._array, "//////////////////////");
			dispatch({ type: SET_PLACES, places: dbResult.rows._array });
		} catch (err) {
			throw err;
		}
	};
};
