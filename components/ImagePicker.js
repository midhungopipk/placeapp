import React, { useState } from "react";
import { View, Text, StyleSheet, Image, Button, Alert } from "react-native";
import * as ImagePicker from "expo-image-picker";
import * as Permissions from "expo-permissions";

const ImgPicker = (props) => {
	const [pickedImage, setPickedImage] = useState();

	const verifyPermissions = async () => {
		// const result = await Permissions.askAsync(Permissions.CAMERA);
		const result = await ImagePicker.requestMediaLibraryPermissionsAsync();
		if (result.status !== "granted") {
			Alert.alert(
				"Insuficient Permissions!",
				"Plese allow Camera to access",
				[{ text: "Okay" }]
			);
			return false;
		}
		return true;
	};

	const takeImageHandler = async () => {
		const hasPermissions = await verifyPermissions();
		if (!hasPermissions) {
			return;
		}
		const image = await ImagePicker.launchCameraAsync({
			quality: 0.5,
			allowsEditing: true,
			aspect: [16, 9],
		});
		setPickedImage(image.uri);
		props.onImageTaken(image.uri);
	};
	
	return (
		<View style={styles.imagePicker}>
			<View style={styles.imagePreview}>
				{!pickedImage ? (
					<Text>No image picked yet.</Text>
				) : (
					<Image style={styles.image} source={{ uri: pickedImage }} />
				)}
			</View>
			<Button
				title="Take Image"
				color={"darkgreen"}
				onPress={takeImageHandler}
			/>
		</View>
	);
};

const styles = StyleSheet.create({
	imagePicker: {
		alignItems: "center",
		marginVertical:20
	},
	imagePreview: {
		width: "100%",
		height: 200,
		marginBottom: 10,
		justifyContent: "center",
		alignItems: "center",
		borderColor: "#ccc",
		borderWidth: 2,
	},
	image: {
		width: "100%",
		height: "100%",
	},
});

export default ImgPicker;
