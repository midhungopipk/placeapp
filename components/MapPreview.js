import React from "react";
import { Image, StyleSheet, TouchableOpacity, View } from "react-native";
import ENV from "../env";
import MapView, { Marker } from "react-native-maps";

const MapPreview = (props) => {
	const mapRegion = {
		latitude: props.location?props.location.lat: 37.78,
		longitude: props.location?props.location.lng:-122.43,
		latitudeDelta: 0.0922,
		longitudeDelta: 0.0421,
	};
	let imagePreviewUrl;

	let markerCoordinates;
	if (props.location) {
		markerCoordinates = {
			latitude: props.location.lat,
			longitude: props.location.lng,
		};
	}

	// if (props.location) {
	// 	imagePreviewUrl = `https://maps.googleapis.com/maps/api/staticmap?center=${props.location.lat},${props.location.lng}&zoom=14&size=400x200&maptype=roadmap&markers=color:red%7Clabel:A%7C${props.location.lat},${props.location.lng}&key=${ENV.googleApiKey}`;

	// }

	return (
		<TouchableOpacity
			onPress={props.onPress}
			style={{ ...styles.mapPreview, ...props.style }}
		>
			{props.location ? (
				// <Image
				// 	style={styles.mapImage}
				// 	source={{ uri: imagePreviewUrl }}
				// />
				<MapView region={mapRegion} style={{width:'100%',height:'100%'}}>
					<Marker
						title="Picked Location"
						coordinate={markerCoordinates}
					></Marker>
				</MapView>
			) : (
				props.children
			)}
		</TouchableOpacity>
	);
};

const styles = StyleSheet.create({
	mapPreview: {
		alignItems: "center",
		justifyContent: "center",
	},
	mapImage: {
		width: "100%",
		height: "100%",
	},
});

export default MapPreview;
