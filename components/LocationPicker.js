import React, { useEffect, useState } from "react";
import {
	View,
	Text,
	Button,
	ActivityIndicator,
	Alert,
	StyleSheet,
} from "react-native";
import * as Location from "expo-location";
import * as Permissions from "expo-permissions";
import MapPreview from "./MapPreview";

const LocationPicker = (props) => {
	const [pickedLocation, setPickedLocation] = useState();
	const [isFetching, setIsFetching] = useState(false);

	const mapPickedLocation = props.navigation.getParam("pickedLocation");
	
	const { onPickedLocation } = props;
	useEffect(() => {
		if (mapPickedLocation) {
			setPickedLocation(mapPickedLocation);
			onPickedLocation(mapPickedLocation);
		}
	}, [mapPickedLocation, onPickedLocation]);

	const verifyPermissions = async () => {
		const result = await Location.requestForegroundPermissionsAsync();
		if (result.status !== "granted") {
			Alert.alert(
				"Insufficient permissions!",
				"you need to grant location permission to acces",
				[{ text: "Okay" }]
			);
			return false;
		}
		return true;
	};
	const getLocationHandler = async () => {
		const hasPermission = await verifyPermissions();
		if (!hasPermission) {
			return;
		}
		try {
			setIsFetching(true);
			const location = await Location.getCurrentPositionAsync({
				timeout: 5000,
			});

			// console.log(location, "//////////////");
			setPickedLocation({
				lat: location.coords.latitude,
				lng: location.coords.longitude,
			});
			props.onPickedLocation({
				lat: location.coords.latitude,
				lng: location.coords.longitude,
			});
		} catch (err) {
			Alert.alert(
				"Could not fetch Location",
				"Please try again later or pick a location on the map",
				[{ text: "Okay" }]
			);
		}
		setIsFetching(false);
	};
	const pickOnMapHandler = () => {
		props.navigation.navigate("Map");
	};
	// console.log(pickedLocation,'hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh');
	return (
		<View style={styles.locationPicker}>
			<MapPreview
				onPress={pickOnMapHandler}
				location={pickedLocation}
				style={styles.mapPreview}
			>
				{isFetching ? (
					<ActivityIndicator size={"large"} color={"blue"} />
				) : (
					<Text>No Location choosen yet</Text>
				)}
			</MapPreview>
			<View style={styles.actions}>
				<Button
					title="Get User Location"
					color={"darkgreen"}
					onPress={getLocationHandler}
				/>
				<Button
					title="Pick on Map"
					color={"darkgreen"}
					onPress={pickOnMapHandler}
				/>
			</View>
		</View>
	);
};
const styles = StyleSheet.create({
	locationPicker: {
		marginBottom: 15,
		alignItems: "center",
	},
	mapPreview: {
		marginBottom: 10,
		width: "100%",
		height: 150,
		borderColor: "#ccc",
		borderWidth: 2,
		// justifyContent: "center",
		// alignItems: "center",
	},
	actions: {
		flexDirection: "row",
		justifyContent: "space-around",
		width: "100%",
	},
});

export default LocationPicker;
